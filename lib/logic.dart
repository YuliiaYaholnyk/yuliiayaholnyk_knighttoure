


int Solution(int x, int y, List<List<int>> position) {
  print('Solution method');
  int t = 0;
  if (x > 0 && y > 1 && position[x - 1][ y - 2] == 0) t++;
  if (x > 0 && y < 6 && position[x - 1][y + 2] == 0) t++;
  if (x > 1 && y > 0 && position[x - 2][y - 1] == 0) t++;
  if (x > 1 && y < 7 && position[x - 2][y + 1] == 0) t++;
  if (x < 7 && y > 1 && position[x + 1][y - 2] == 0) t++;
  if (x < 7 && y < 6 && position[x + 1][y + 2] == 0) t++;
  if (x < 6 && y > 0 && position[x + 2][y - 1] == 0) t++;
  if (x < 6 && y < 7 && position[x + 2][y + 1] == 0) t++;
  return t;
}
List<int> knightTourSolution(int x, int y) {
  // List<List<int>> position = List.generate(8, (i) => List(8),);
  List<List<int>> position = new List<List<int>>();
  for (var i = 0; i < 8; i++) {
    List<int> list = new List<int>();
    for (var j = 0; j < 8; j++) {
      list.add(0);
    }
    position.add(list);
  }
  int i = 0;
  int j = 0;
  int countOfMoves = 0;
  int tmp, tmp1;
  print('KNIGTH  $position');
  while(countOfMoves < 64) {
    countOfMoves++;
    position[x][y] = countOfMoves;
    tmp = 9;
    if (x > 0 && y > 1 && position[x - 1][ y - 2] == 0)
    {
      tmp1 = Solution(x - 1, y - 2, position);
      if (tmp1 < tmp) { i = x - 1; j = y - 2; tmp = tmp1; }
    }
    if (x > 0 && y < 6 && position[x - 1][y + 2] == 0)
    {
      tmp1 = Solution(x - 1, y + 2, position);
      if (tmp1 < tmp) { i = x - 1; j = y + 2; tmp = tmp1; }
    }
    if (x > 1 && y > 0 && position[x - 2][y - 1] == 0)
    {
      tmp1 = Solution(x - 2, y - 1, position);
      if (tmp1 < tmp) { i = x - 2; j = y - 1; tmp = tmp1; }
    }
    if (x > 1 && y < 7 && position[x - 2][y + 1] == 0)
    {
      tmp1 = Solution(x - 2, y + 1, position);
      if (tmp1 < tmp) { i = x - 2; j = y + 1; tmp = tmp1; }
    }
    if (x < 7 && y > 1 && position[x + 1][y - 2] == 0)
    {
      tmp1 = Solution(x + 1, y - 2, position);
      if (tmp1 < tmp) { i = x + 1; j = y - 2; tmp = tmp1; }
    }
    if (x < 7 && y < 6 && position[x + 1][y + 2] == 0)
    {
      tmp1 = Solution(x + 1, y + 2, position);
      if (tmp1 < tmp) { i = x + 1; j = y + 2; tmp = tmp1; }
    }
    if (x < 6 && y > 0 && position[x + 2][y - 1] == 0)
    {
      tmp1 = Solution(x + 2, y - 1, position);
      if (tmp1 < tmp) { i = x + 2; j = y - 1; tmp = tmp1; }
    }
    if (x < 6 && y < 7 && position[x + 2][y + 1] == 0)
    {
      tmp1 = Solution(x + 2, y + 1, position);
      if (tmp1 < tmp) { i = x + 2; j = y + 1; tmp = tmp1; }
    }
    x = i;
    y = j;
  }
  for (i = 0; i < 8; i++)
  {
    for (j = 0; j < 8; j++)
      print("\t${position[i][j]}");
    print("\n");
  }

  List<int> result = [];
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) result.add(position[i][j]);
  }
  print(result);
  return result;

}