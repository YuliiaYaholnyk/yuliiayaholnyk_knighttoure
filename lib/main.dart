import 'package:flutter/material.dart';
import 'logic.dart';

List<int> position;

void main() {
  position = knightTourSolution(5, 5);
  //print(position[0][(0/8).round() -1]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double value = 0;
  double value2 = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Testing widgets'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: GridView.builder(
                itemCount: 64,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 8),
                itemBuilder: (BuildContext context, int i) {
                  if ((i / 8.0 >= 0 && i / 8.0 < 1) ||
                      (i / 8.0 >= 2 && i / 8.0 < 3) ||
                      (i / 8.0 >= 4 && i / 8.0 < 5) ||
                      (i / 8.0 >= 6 && i / 8.0 < 7)) {
                    if (i % 2 != 0) {
                      // print(position[][j]);
                      return Container(
                        child: position.isEmpty ? Text('') : Text(position[i].toString(), style: TextStyle(fontSize: 20, color: Colors.white)),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.black,
                          border: Border.all(
                            width: 2.0,
                            color: Colors.white,
                          ),
                        ),
                      );
                    }
                    return Container(
                      child: position.isEmpty
                          ? Text('')
                          : Text(
                              position[i].toString(),
                              style: TextStyle(fontSize: 20, color: Colors.black),
                            ),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(width: 2.0, color: Colors.black),
                      ),
                    );
                  } else if ((i / 8.0 >= 1 && i / 8.0 < 2) ||
                      (i / 8.0 >= 3 && i / 8.0 < 4) ||
                      (i / 8.0 >= 5 && i / 8.0 < 6) ||
                      (i / 8.0 >= 7 && i / 8.0 < 8)) {
                    if (i % 2 == 0) {
                      return Container(
                        child: position.isEmpty
                            ? Text('')
                            : Text(
                                position[i].toString(),
                                style: TextStyle(fontSize: 20, color: Colors.white),
                              ),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.black,
                          border: Border.all(
                            width: 2.0,
                            color: Colors.black,
                          ),
                        ),
                      );
                    }
                    return Container(
                      child: position.isEmpty
                          ? Text('')
                          : Text(
                              position[i].toString(),
                              style: TextStyle(fontSize: 20, color: Colors.black),
                            ),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          width: 2.0,
                          color: Colors.black,
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              ),
            ),
            Slider(
              value: value,
              min: 0,
              max: 7,
              divisions: 7,
              onChanged: (Newvalue) {
                setState(() {
                  value = Newvalue;
                });
              },
              label: '$value',
            ),
            Slider(
              value: value2,
              min: 0,
              max: 7,
              divisions: 7,
              onChanged: (Newvalue) {
                setState(() {
                  value2 = Newvalue;
                });
              },
              label: '$value2',
            ),
            RaisedButton(
              child: Text('GO'),
              onPressed: () {
                setState(() {
                  position = knightTourSolution(value.round(), value2.round());
                });
              },
            )
          ],
        ));
  }
}
